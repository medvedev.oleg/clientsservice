﻿using Domain.Entities;

namespace Infrastructure.Abstractions
{
    /// <summary>
    /// Репозиторий работы с правами.
    /// </summary>
    public interface IRoleRepository : EfRepository<Role>
    {
 
    }
}