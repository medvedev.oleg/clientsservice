﻿using Domain.Entities;

namespace Infrastructure.Abstractions
{
    /// <summary>
    /// Репозиторий работы с потльзователями.
    /// </summary>
    public interface IUserRepository : EfRepository<User>
    {
 
    }
}