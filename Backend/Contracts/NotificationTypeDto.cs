﻿namespace Infrastructure.Contracts
{
    public class NotificationTypeDto
    {
        public bool? SMS { get; set; }
        public bool? Email { get; set; }
        public bool? Telegram { get; set; }
    }
}
