﻿using Domain.Entities;
using MongoDB.Bson;

namespace Infrastructure.Contracts
{
    public class RoleDto
    {
        public Guid? EntityId { get; set; }
        public GroupRoles GroupRole { get; set; }
        public string Description { get; set; }
    }
}
