﻿using Domain.Entities.Enums;
using MongoDB.Bson;
using System;

namespace Infrastructure.Contracts
{
    public class UserDto
    {
        public Guid? EntityId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public GroupRoles Role { get; set; }
        public NotificationTypeDto NotificationType { get; set; }
        public bool Delete { get; set; }
        public byte[] Avatar { get; set; }
        public DateTime Created { get; set; }
    }
}
