﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.Entities
{
    public class BaseEntity
    {        
        public ObjectId Id { get; set; }
        public Guid? EntityId { get; set; }
    }
}