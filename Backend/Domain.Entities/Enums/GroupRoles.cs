﻿namespace Domain.Entities.Enums
{
    public enum GroupRoles
    {
        User, //"Пользователь"
        Editor, // "Редактор"
        Admin, // "Администратор"
    }
}
