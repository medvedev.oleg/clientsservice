﻿using MongoDB.Bson;

namespace Domain.Entities.Enums
{
    [Flags]
    public enum NotificationType
    {
        Empty = 0,
        SMS = 1,
        Email = 2,
        Telegram = 4,
    }
}
