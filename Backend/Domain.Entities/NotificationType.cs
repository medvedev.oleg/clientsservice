﻿using Domain.Entities.Enums;
using MongoDB.Bson;

namespace Domain.Entities
{
    public class NotificationType
    {
        public bool? SMS { get; set; }
        public bool? Email { get; set; }
        public bool? Telegram { get; set; }
    }
}

 
