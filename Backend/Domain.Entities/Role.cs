﻿namespace Domain.Entities
{
    public class Role : BaseEntity
    {
        public GroupRoles GroupRole { get; set; }
        public string Description { get; set; }
    }
}
