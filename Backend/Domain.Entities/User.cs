﻿using Domain.Entities.Enums;
using MongoDB.Bson;

namespace Domain.Entities
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public GroupRoles Role { get; set; }
        public NotificationType NotificationType { get; set; }
        public byte[] Avatar { get; set; }
        public bool Delete { get; set; }
        public DateTime Created { get; set; }

    }
}
