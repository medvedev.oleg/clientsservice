﻿using Domain.Entities;
using Domain.Entities.Enums;
using Microsoft.EntityFrameworkCore;
using MongoDB.EntityFrameworkCore.Extensions;

namespace ClientsService.EntityFramework
{
    /// <summary>
    /// Контекст.
    /// </summary>
    public class DatabaseContext : DbContext
    {

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //try
            //{
            //    Database.EnsureDeleted();
            //    Database.EnsureCreated();
            //}catch (Exception ex)
            //{
            //    throw; 
            //}
        }
         
        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<User> User { get; set; }
 

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
             modelBuilder.Entity<User>().ToCollection("users");
         }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }
    }
}