﻿
using Domain.Entities;
using Domain.Entities.Enums;
using MongoDB.Bson;

namespace FakeDataFactory
{
    public static class FakeDataFactoryData
    {
        public static IEnumerable<User> Users => new List<User>()
        {
            new User()
            {
                Id = ObjectId.GenerateNewId(),
                EntityId = Guid.NewGuid(),
                FirstName = "Иван",
                LastName = "Сергеев",
                Email = "asd@asd.com",
                Role = GroupRoles.User,
             },
            new User()
            {
                Id = ObjectId.GenerateNewId(),
                EntityId = Guid.NewGuid(),
                FirstName = "Петр",
                LastName = "Андреев",
                Email = "as777@sdf.com",
                Role = GroupRoles.Admin,
              },
            new User()
            {
                Id = ObjectId.GenerateNewId(),
                EntityId = Guid.NewGuid(),
                FirstName = "Вячеслав",
                LastName = "Селевеев",
                Email = "nana@sdf.com",
                Role = GroupRoles.Editor,
              },
        };

       

    }
}