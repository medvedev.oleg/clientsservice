﻿using ClientsService.EntityFramework;
using Domain.Entities;
using Infrastructure.Abstractions;
using Infrastructure.Repository;

namespace Infrastructure.RoleRepository
{
    /// <summary>
    /// Репозиторий работы с ролями.
    /// </summary>
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(DatabaseContext context) : base(context)
        {             
        }

    }
}
