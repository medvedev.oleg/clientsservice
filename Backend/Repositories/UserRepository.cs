﻿using ClientsService.EntityFramework;
using Domain.Entities;
using Infrastructure.Abstractions;
using Infrastructure.Repository;

namespace Infrastructure.UserRepository
{
    /// <summary>
    /// Репозиторий работы с пользователями.
    /// </summary>
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DatabaseContext context) : base(context)
        {             
        }
    }
}
