﻿using Infrastructure.Contracts;

namespace Services
{
    /// <summary>
    /// Интерфейс сервиса работы с ролями.
    /// </summary>
    public interface IRoleService
    {
        /// <summary>
        /// Получить все роли.
        /// </summary> 
        Task<List<RoleDto>> GetAllAsync();

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="creatingUserDto"> ДТО</param>
        Task<Guid> CreateAsync(RoleDto creatingRoleDto);
    }
}