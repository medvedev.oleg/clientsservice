﻿using Infrastructure.Contracts;

namespace Services
{
    /// <summary>
    /// Интерфейс сервиса работы с пользователями.
    /// </summary>
    public interface IUserService
    {

        /// <summary>
        /// Получить все.
        /// </summary> 
        Task<List<UserDto>> GetAllAsync();


        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО</returns>
        Task<UserDto> GetByIdAsync(Guid id);


        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="email"> Идентификатор. </param>
        /// <returns> ДТО</returns>
        Task<UserDto> GetByEmailAsync(string email);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="creatingUserDto"> ДТО</param>
        Task<Guid> CreateAsync(UserDto creatingUserDto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id"> Иентификатор. </param>
        /// <param name="updatingUserDto"> ДТО</param>
        Task UpdateAsync(Guid id, UserDto updatingUserDto);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        Task DeleteAsync(Guid id);
    }
}