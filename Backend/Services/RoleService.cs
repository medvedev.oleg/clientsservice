﻿using Domain.Entities;
using Infrastructure.Abstractions;
using Infrastructure.Contracts;

namespace Services
{
    /// <summary>
    /// Cервис работы с курсами.
    /// </summary>
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить все роли.
        /// </summary> 
        public async Task<List<RoleDto>> GetAllAsync()
        {
            var roles = await _roleRepository.GetAllAsync();
            var listRoles = roles.Select(r => new RoleDto()
            {
                EntityId = (Guid)r.EntityId,
                GroupRole = r.GroupRole,
                Description = r.Description,
            }).ToList();

            return listRoles;
        }


        public async Task<Guid> CreateAsync(RoleDto creatingRoleDto)
        {
            var role = new Role()
            {
                EntityId = Guid.NewGuid(),
                Description = creatingRoleDto.Description,
                GroupRole = creatingRoleDto.GroupRole,                
            };

            _roleRepository.Add(role);
            await _roleRepository.SaveChangesAsync();

            return (Guid)role.EntityId;
        }
    }
}