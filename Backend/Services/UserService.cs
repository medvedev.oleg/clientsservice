﻿using Domain.Entities;
using Domain.Entities.Enums;
using Infrastructure.Abstractions;
using Infrastructure.Contracts;

namespace Services
{
    /// <summary>
    /// Cервис работы с курсами.
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Получить все
        /// </summary> 
        public async Task<List<UserDto>> GetAllAsync()
        {
            var users = await _userRepository.GetAllAsync();
            users.Sort((x, y) => DateTime.Compare(x.Created, y.Created));
            users.Reverse();

            var listUsers = users.Select(user => new UserDto()
            {
                EntityId = user.EntityId,
                FirstName = user.FirstName,                
                Email = user.Email,
                LastName = user.LastName,
                Role = user.Role,
                Delete = user.Delete,
                NotificationType = new NotificationTypeDto()
                {
                    Email = user.NotificationType.Email,
                    SMS = user.NotificationType.SMS,
                    Telegram = user.NotificationType.Telegram
                },
                Avatar = user.Avatar,
            }).ToList();

            return listUsers;
        }

        public async Task<UserDto> GetByIdAsync(Guid id)
        {
            var users = await _userRepository.GetAllAsync();
            var user = users.Find(n => n.EntityId == id);

            if (user == null)
            {
                throw new Exception($"Пользователь с идентфикатором {id} не найдено");
            }

            return new UserDto()
            {
                EntityId = user.EntityId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Role = user.Role,
                NotificationType = new NotificationTypeDto()
                {
                    Email = user.NotificationType.Email,
                    SMS = user.NotificationType.SMS,
                    Telegram = user.NotificationType.Telegram
                },
                Avatar = user.Avatar,
            };
        }

        public async Task<UserDto> GetByEmailAsync(string email)
        {
            var users = await _userRepository.GetAllAsync();
            var user = users.Find(n => n.Email == email);

            if (user == null)
            {
                throw new Exception($"Пользователь с email {email} не найдено");
            }

            return new UserDto()
            {
                EntityId = user.EntityId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Role = user.Role,
                NotificationType = new NotificationTypeDto()
                {
                    Email = user.NotificationType.Email,
                    SMS = user.NotificationType.SMS,
                    Telegram = user.NotificationType.Telegram
                },
                Avatar = user.Avatar,
            };
        }
        public async Task<Guid> CreateAsync(UserDto creatingUserDto)
        {
            var user = new User()
            {
                EntityId = Guid.NewGuid(),
                FirstName = creatingUserDto.FirstName,
                LastName = creatingUserDto.LastName,
                Email = creatingUserDto.Email,
                Role = creatingUserDto.Role,
                NotificationType = new NotificationType()
                {
                    Email = creatingUserDto.NotificationType.Email,
                    SMS = creatingUserDto.NotificationType.SMS,
                    Telegram = creatingUserDto.NotificationType.Telegram
                },
                Avatar = creatingUserDto.Avatar,
            };

            _userRepository.Add(user);
            await _userRepository.SaveChangesAsync();

            return (Guid)user.EntityId;
        }

        public async Task DeleteAsync(Guid id)
        {
            var users = await _userRepository.GetAllAsync();
            var user = users.Find(n => n.EntityId == id);

            if (user == null)
            {
                throw new Exception($"Пользователь с идентфикатором {id} не найдено");
            }

            user.Delete = true;
            await _userRepository.SaveChangesAsync();
        }

        public async Task UpdateAsync(Guid id, UserDto updatingUserDto)
        {
            var users = await _userRepository.GetAllAsync();
            var user = users.Find(n => n.EntityId == id);

            if (user == null)
            {
                throw new Exception($"Пользователь с идентфикатором {id} не найдено");
            }

            user.FirstName = updatingUserDto.FirstName;
            user.LastName = updatingUserDto.LastName;
            user.Role = updatingUserDto.Role;
            user.Delete = updatingUserDto.Delete;
            user.Email = updatingUserDto.Email;
            user.NotificationType = new NotificationType()
            {
                Email = updatingUserDto.NotificationType.Email,
                SMS = updatingUserDto.NotificationType.SMS,
                Telegram = updatingUserDto.NotificationType.Telegram
            };
            user.Avatar = updatingUserDto.Avatar;

            _userRepository.Update(user);
            await _userRepository.SaveChangesAsync();
        }
    }
}