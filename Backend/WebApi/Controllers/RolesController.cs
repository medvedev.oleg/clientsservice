﻿using Infrastructure.Contracts;
using Microsoft.AspNetCore.Mvc;
using Services;
using Swashbuckle.AspNetCore.Annotations;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RolesController : ControllerBase
    {

        private readonly ILogger<RolesController> _logger;
        private readonly IRoleService _service;

        public RolesController(ILogger<RolesController> logger, IRoleService service)
        {
            _logger = logger;
            _service = service;
        }

        /// <summary>
        /// Получить все роли пользователей 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet()]
        [SwaggerOperation(
        Summary = "Получить все роли пользователей",
        Description = "Получить все роли пользователей")]
        public async Task<IActionResult> GetRolesAsync()
        {
            var roles = await _service.GetAllAsync();

            var listRoles = roles.Select(r =>
                new RoleDto()
                {
                    EntityId = r.EntityId,
                    Description = r.Description,
                    GroupRole = r.GroupRole,
                }).ToList();

            return Ok(listRoles);
        }

        /// <summary>
        /// Создать роль
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost()]
        public async Task<IActionResult> CreateUserAsync(ModelsRole data)
        {
            return Ok(await _service.CreateAsync(new RoleDto()
            {
                EntityId = Guid.NewGuid(),
                Description = data.Description,
                GroupRole = (Domain.Entities.GroupRoles)data.GroupRole,                
            }));

        }

    }
}
