﻿using Domain.Entities.Enums;
using Infrastructure.Contracts;
using Microsoft.AspNetCore.Mvc;
using Services;
using Swashbuckle.AspNetCore.Annotations;
using System;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {

        private readonly ILogger<UsersController> _logger;
        private readonly IUserService _service;

        private NotificationTypeDto baseNotificationType = new NotificationTypeDto()
        {
            Email = false,
            SMS = false,
            Telegram = false,
        };

        public UsersController(ILogger<UsersController> logger, IUserService service)
        {
            _logger = logger;
            _service = service;
        }
         

        /// <summary>
        /// Получить данные всех пользователей
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet()]
        [SwaggerOperation(
        Summary = "Возвращает данные всех пользователей",
        Description = "Возвращает данные всех пользователей")]
        public async Task<IActionResult> GetUsersAsync()
        {
            var users = await _service.GetAllAsync();

            var listUsers = users.Select(user =>
            new ModelsUserResponse()
            {
                EntityId = user.EntityId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Role = (Enums.GroupRoles)user.Role,
                Delete = user.Delete,
                NotificationType = user.NotificationType,
                Avatar = user.Avatar,
            }).ToList();

            return Ok(listUsers);
        }

        /// <summary>
        /// Получить пользователя по id
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet("{id:Guid}")]
        public async Task<IActionResult> GetUserAsync(Guid id)
        {
            var user = await _service.GetByIdAsync(id);
            return Ok(new ModelsUserResponse()
            {
                EntityId = user.EntityId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Role = (Enums.GroupRoles)user.Role,
                Delete = user.Delete,
                NotificationType = user.NotificationType,
                Avatar = user.Avatar,
            });
        }

        /// <summary>
        /// Получить пользователя по email
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet("email/{email}")]
        public async Task<IActionResult> GetUserAsync(string email)
        {
            var user = await _service.GetByEmailAsync(email);
            return Ok(new ModelsUserResponse()
            {
                EntityId = user.EntityId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Role = (Enums.GroupRoles)user.Role,
                Delete = user.Delete,
                NotificationType = user.NotificationType,
                Avatar = user.Avatar,
            });
        }


        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost()]
        public async Task<IActionResult> CreateUserAsync(ModelsUser data)
        {
            var type = baseNotificationType;

            if (data.NotificationType.SMS != null)
            {
                type.SMS = data.NotificationType.SMS;
            }

            if (data.NotificationType.Telegram != null)
            {
                type.Telegram = data.NotificationType.Telegram;
            }

            if (data.NotificationType.Email != null)
            {
                type.Email = data.NotificationType.Email;
            }


            var user = new UserDto()
            {
                EntityId = Guid.NewGuid(),
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Role = (GroupRoles)data.Role,
                NotificationType = type,
                Created = new DateTime(),
            };


            if (data.Avatar != null)
            {
                byte[] imageData = null;
                // считываем переданный файл в массив байтов
                using (var binaryReader = new BinaryReader(data.Avatar.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)data.Avatar.Length);
                }
                // установка массива байтов
                user.Avatar = imageData;
            }

            return Ok(await _service.CreateAsync(user));
        }

        /// <summary>
        /// Изменить пользователя
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPut("{id:Guid}")]
        public async Task<IActionResult> EditUserAsync(Guid id, ModelsEditUser data)
        {
            var user = await _service.GetByIdAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            if (data.FirstName != null)
            {
                user.FirstName = data.FirstName;
            }

            if (data.LastName != null)
            {
                user.LastName = data.LastName;
            }

            if (data.Role != null)
            {
                user.Role = (GroupRoles)data.Role;
            }

            if (data.Email != null)
            {
                user.Email = data.Email;
            }

            if (data.NotificationType != null)
            {
                if (data.NotificationType.SMS != null)
                {
                    user.NotificationType.SMS = data.NotificationType.SMS;
                }

                if (data.NotificationType.Telegram != null)
                {
                    user.NotificationType.Telegram = data.NotificationType.Telegram;
                }

                if (data.NotificationType.Email != null)
                {
                    user.NotificationType.Email = data.NotificationType.Email;
                }
            }


            if (data.Avatar != null)
            {
                byte[] imageData = null;
                // считываем переданный файл в массив байтов
                using (var binaryReader = new BinaryReader(data.Avatar.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)data.Avatar.Length);
                }
                // установка массива байтов
                user.Avatar = imageData;
            }

            await _service.UpdateAsync(id, user);

            return Ok();
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpDelete("{id:Guid}")]
        public async Task<IActionResult> DeleteUserAsync(Guid id)
        {
            await _service.DeleteAsync(id);
            return Ok();
        }

    }
}
