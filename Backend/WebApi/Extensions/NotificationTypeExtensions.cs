﻿using Domain.Entities;
using Domain.Entities.Enums;
using Infrastructure.Contracts;
using Microsoft.AspNetCore.Mvc;
using Services;
using Swashbuckle.AspNetCore.Annotations;
using System.Text;
using WebApi.Models;

namespace WebApi.Extensions
{

    public static class NotificationTypeExtensions
    {

        /// <summary>
        ///     Установка флага в положение вкл.
        /// </summary>
        public static NotificationType On(this NotificationType prop, NotificationType value)
        {
            return prop | value;
        }

        /// <summary>
        ///     Установка флага в положение выкл.
        /// </summary>
        public static NotificationType Off(this NotificationType prop, NotificationType value)
        {
            return prop & ~value;
        }

        public static List<string> GetDescription(this NotificationType src)
        {
            var str = new List<string>();
            if (src.HasFlag(NotificationType.SMS))
            {
                str.Add("SMS");
            }

            if (src.HasFlag(NotificationType.Email))
            {
                str.Add("Email");
            }

            if (src.HasFlag(NotificationType.Telegram))
            {
                str.Add("Telegram");
            }

            return str;
        }

        public static NotificationTypeDto GetNotificationType(this NotificationType src)
        {
            var notificationType = new NotificationTypeDto();
            
            //notificationType.SMS = false;
            if (src.HasFlag(NotificationType.SMS))
            {
                notificationType.SMS = true;
            }
            

            if (src.HasFlag(NotificationType.Email))
            {
                notificationType.Email = true;
            }

            if (src.HasFlag(NotificationType.Telegram))
            {
                notificationType.Telegram = true;
            }

            return notificationType;
        }
    }
}
