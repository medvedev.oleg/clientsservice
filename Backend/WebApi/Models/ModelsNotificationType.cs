﻿namespace WebApi.Models
{
    public class ModelsNotificationType
    {
        public bool? SMS { get; set; }
        public bool? Email { get; set; }
        public bool? Telegram { get; set; }
    }
}
