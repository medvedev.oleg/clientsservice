﻿using WebApi.Enums;

namespace WebApi.Models
{
    public class ModelsRole
    {
        public GroupRoles GroupRole { get; set; }
        public string Description { get; set; }
    }
}
