﻿using Domain.Entities;
using System.ComponentModel.DataAnnotations;
using WebApi.Enums;

namespace WebApi.Models
{
    public class ModelsUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }        
        public ModelsNotificationType NotificationType { get; set; }
        public IFormFile? Avatar { get; set; }
        public GroupRoles Role { get; set; }
    }
}
