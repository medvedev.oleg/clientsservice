﻿using Infrastructure.Contracts;
using WebApi.Enums;

namespace WebApi.Models
{
    public class ModelsUserResponse
    {
        public Guid? EntityId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public GroupRoles Role { get; set; }
        public NotificationTypeDto NotificationType { get; set; }
        public bool Delete { get; set; }
        public byte[] Avatar { get; set; }
    }
}
