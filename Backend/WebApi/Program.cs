﻿using ClientsService.EntityFramework;
using Infrastructure.Abstractions;
using Infrastructure.UserRepository;
using Microsoft.EntityFrameworkCore;
using Services;
using System.Text.Json.Serialization;

namespace WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var MyAllowSpecificClientOrigins = "_myAllowSpecificClientOrigins";

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificClientOrigins,
                                  policy =>
                                  {
                                      policy.AllowAnyOrigin()
                                            .AllowAnyHeader()
                                            .AllowAnyMethod();
                                  });
            });

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            ConfigurationManager configuration = builder.Configuration;
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            var databaseName = configuration.GetConnectionString("DatabaseName");

            builder.Services.AddDbContext<DatabaseContext>(options => options.UseMongoDB(connectionString ?? "", databaseName ?? "").EnableSensitiveDataLogging());

            builder.Services.AddScoped<IUserRepository, UserRepository>();
            builder.Services.AddScoped<IUserService, UserService>();

            builder.Services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });

            var app = builder.Build();

            app.UseCors(MyAllowSpecificClientOrigins);

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}
