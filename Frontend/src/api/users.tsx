import axios from "axios";
import { UserResponse } from "../models/User";
import { CreateUserRequest } from "../models/CreateUserRequest";

const API = "https://localhost:7128"

export const getUsers = (): Promise<{data: UserResponse}> => axios({ method: 'get', url: `${API}/Users` });
export const createUser = (data: FormData): Promise<{data: string}> => axios({ method: 'post', url: `${API}/Users`, data });
 