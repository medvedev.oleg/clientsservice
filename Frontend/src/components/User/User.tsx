import { FC } from 'react';
import './styles.css';
import { User } from '../../models/User';

type UserProps = Partial<User>;

export const UserWidget: FC<UserProps> = (props) => {
  return (
    <div className="user-widget">
      <div className="user-widget__row">
        <div className="user-widget__lable">avatar</div>
        <div className="user-widget__value">{props.avatar && <img src={`data:image/png;base64, ${props.avatar}`} width="100%" height="100%" style={{ objectFit: "fill" }} />}</div>
      </div>
      <div className="user-widget__row">
        <div className="user-widget__lable">email</div>
        <div className="user-widget__value">{props.email}</div>
      </div>
      <div className="user-widget__row">
        <div className="user-widget__lable">entityId</div>
        <div className="user-widget__value">{props.entityId}</div>
      </div>
      <div className="user-widget__row">
        <div className="user-widget__lable">firstName</div>
        <div className="user-widget__value">{props.firstName}</div>
      </div>
      <div className="user-widget__row">
        <div className="user-widget__lable">lastName</div>
        <div className="user-widget__value">{props.lastName}</div>
      </div>
      <div className="user-widget__row">
        <div className="user-widget__lable">notificationType</div>
        <div className="user-widget__value">{JSON.stringify(props.notificationType).replace(',"', ', "')}</div>
      </div>
      <div className="user-widget__row">
        <div className="user-widget__lable">role</div>
        <div className="user-widget__value">{props.role}</div>
      </div>
    </div>
  );
}

