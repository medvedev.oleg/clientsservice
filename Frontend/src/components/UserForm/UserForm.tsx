import { FC, useRef, useState } from 'react';
import './styles.css';
import LoadingButton from '@mui/lab/LoadingButton';
import { createUser } from '../../api/users';

export const UserForm: FC<{ successCallback: Function, closePane: Function }> = ({ successCallback, closePane }) => {
  const [isLoading, setIsLoading] = useState(false)

  const refAvatar = useRef<HTMLInputElement>(null);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [notificationTypeSMS, setNotificationTypeSMS] = useState("");
  const [notificationTypeEmail, setNotificationTypeEmail] = useState("");
  const [notificationTypeTelegram, setNotificationTypeTelegram] = useState("");
  const [role, setRole] = useState("");


  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    setIsLoading(true);

    const formData = new FormData();
    firstName && formData.append("firstName", firstName);
    lastName && formData.append("lastName", lastName);
    email && formData.append("email", email);
    notificationTypeSMS && formData.append("NotificationType.SMS", `${Boolean(notificationTypeSMS)}`);
    notificationTypeEmail && formData.append("NotificationType.Email", `${Boolean(notificationTypeEmail)}`);
    notificationTypeTelegram && formData.append("NotificationType.Telegram", `${Boolean(notificationTypeTelegram)}`);
    role && formData.append("role", role);
    if (refAvatar.current?.files) {
      (refAvatar.current?.files?.length > 0) && formData.append("avatar", refAvatar.current?.files[0]);
    }

    //Чтобы показать крутилку на кнопке
    setTimeout(() => {
      const createRequest = createUser(formData);

      createRequest
        .then((response) => void successCallback(response.data))
        .catch((error) => void alert(error.message))
        .finally(() => void setIsLoading(false))
    }, 1111)

    event.stopPropagation();
    event.preventDefault();

  }


  return (
    <div className="form-widget">
      <div onClick={() => { closePane?.(); }} className='form-widget__close'>Закрыть</div>
      <form className="commentForm" onSubmit={handleSubmit}>
        <div className="form-group">
          <div className="form-row">
            <div className="form-lable">FirstName<span style={{ color: "red" }}>*</span></div>
            <input
              required
              type="text"
              value={firstName}
              onChange={(event) => setFirstName(event.currentTarget.value)}
            />
          </div>

          <div className="form-row">
            <div className="form-lable">LastName<span style={{ color: "red" }}>*</span></div>
            <input
              required
              type="text"
              value={lastName}
              onChange={(event) => setLastName(event.currentTarget.value)}
            />

          </div>

          <div className="form-row">
            <div className="form-lable">Email<span style={{ color: "red" }}>*</span></div>
            <input
              required
              type="text"
              value={email}
              onChange={(event) => setEmail(event.currentTarget.value)}
            />

          </div>

          <div className="form-row">
            <div className="form-lable">NotificationTypeNotificationType.SMS<span style={{ color: "red" }}>*</span></div>
            <select required onChange={(event) => setNotificationTypeSMS(event.target.value)} >
              <option value=""></option>
              <option value={1}>true</option>
              <option value={0}>false</option>
            </select>

          </div>

          <div className="form-row">
            <div className="form-lable">NotificationTypeNotificationType.Email<span style={{ color: "red" }}>*</span></div>
            <select required onChange={(event) => setNotificationTypeEmail(event.target.value)} >
              <option value=""></option>
              <option value={1}>true</option>
              <option value={0}>false</option>
            </select>

          </div>

          <div className="form-row">
            <div className="form-lable">NotificationTypeNotificationType.Telegram<span style={{ color: "red" }}>*</span></div>
            <select required onChange={(event) => setNotificationTypeTelegram(event.target.value)} >
              <option value=""></option>
              <option value={1}>true</option>
              <option value={0}>false</option>
            </select>

          </div>

          <div className="form-row">
            <div className="form-lable">Role</div>
            <select onChange={(event) => setRole(event.target.value)} >
              <option value=""></option>
              <option value={0}>User</option>
              <option value={1}>Editor</option>
              <option value={2}>Admin</option>
            </select>

          </div>

          <div className="form-row">
            <div className="form-lable">Avatar</div>
            <input type="file" id="avatar" accept=".png" ref={refAvatar} />
          </div>


          <LoadingButton
            loading={isLoading}
            type="submit"
            className="section-buttonsbar__button"
            variant="contained">
            Create user
          </LoadingButton>
        </div>
      </form>
    </div>
  );
}
