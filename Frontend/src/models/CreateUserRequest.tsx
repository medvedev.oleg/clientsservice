import { User } from "./User";
  
export type CreateUserRequest =  Omit<User, "entityId" | "avatar" > & {
  avatar: File,
};
