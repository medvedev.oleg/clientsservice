type ModelNotificationType = {
  SMS?: boolean,
  Email?: boolean,
  Telegram?: boolean,
}

export type NotificationType = ModelNotificationType | null;
