export type User = {
  entityId: string,
  firstName: string,
  lastName: string,
  role: string,
  notificationType: {
    ms: boolean,
    email: boolean,
    telegram: boolean
  },
  avatar: string,
  email: string,
}



export type UserResponse = User[];
