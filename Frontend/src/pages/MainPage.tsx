import { FC, useRef, useState } from 'react';
import { UserWidget } from '../components/User';
import { UserResponse } from '../models/User';
import LoadingButton from '@mui/lab/LoadingButton';
import { getUsers } from '../api/users';
import { UserForm } from '../components/UserForm';


export const MainPage: FC = () => {
  const panelCreateUser = useRef(null);

  const [isLoading, setIsLoading] = useState(false)
  const [userFromApi, setUserFromApi] = useState<UserResponse>()

  const getRandomuserFromApi = async () => {
    setIsLoading(true);

    //Чтобы показать крутилку на кнопке
    setTimeout(() => {
      const fetchUsers = getUsers();

      fetchUsers
        .then((response) => void setUserFromApi(response.data))
        .catch((error) => void alert(error.message))
        .finally(() => void setIsLoading(false))
    }, 1111)
  }

  const tooglePaneCreateUser = () => {
    panelCreateUser.current && (panelCreateUser.current as HTMLDivElement).classList.toggle("visible")
  }


  return (
    <div className="section">

      <div className="section-buttonsbar">
        <LoadingButton
          loading={isLoading}
          className="section-buttonsbar__button"
          onClick={getRandomuserFromApi}
          variant="contained">
          Get /Users
        </LoadingButton>

        <LoadingButton
          className="section-buttonsbar__button"
          onClick={tooglePaneCreateUser}
          variant="contained">
          Toggle form user
        </LoadingButton>
      </div>

      <div className="section-user">
        {userFromApi?.map((props) => <UserWidget {...props} />)}
      </div>


      <div className="panel-create-user invisible" ref={panelCreateUser}>
        <UserForm successCallback={getRandomuserFromApi} closePane={tooglePaneCreateUser} />
      </div>

    </div>
  );
}

